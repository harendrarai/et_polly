package com.cron;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.Constants;
import com.util;
import com.Util.PollyUtil;
import com.amazonaws.services.polly.model.OutputFormat;
import com.model.QuickReadBody;
import com.model.QuickReadFeed;
import com.service.CmsService;
import com.service.PollyService;
import com.til.et.mongo.service.MongoService;
import com.til.et.polly.config.JdbcProperty;
import com.til.et.polly.dao.AudioService;
import com.til.et.polly.dto.SlikeParameterDto;

@Component
public class PollyCron {
	private static Logger logger = LogManager.getLogger(PollyCron.class);
	 
	@Autowired
	PollyService pollyService;

	@Autowired
	CmsService cmsService;
	
	@Autowired
	AudioService  audioService;
	
	@Autowired
	MongoService mongo;
	
	@Autowired
	JdbcProperty property;

	@Scheduled(cron = "0 0/15 * * * *")
	public void CronJobPolly() {

		List<QuickReadFeed> QuickReadFeedList = cmsService.cmsDataList(property.QUICKPLIST);

		for (QuickReadFeed quickReadFeed : QuickReadFeedList) {
			try {
				if (!Constants.uniqueMsids.contains(quickReadFeed.getId())) {
					
					Constants.uniqueMsids.add(quickReadFeed.getId());
					logger.info("quickReadFeed  Header");
					String bodyURL = quickReadFeed.getWu();
					bodyURL = bodyURL.replace("https://economictimes.indiatimes.com","http://www1.economictimes.indiatimes.com/xml");
					
					QuickReadBody parsedCMSdata = util.parseData(bodyURL);
					String audioData = parsedCMSdata.getTitle()+"."+parsedCMSdata.getBodyText();
					pollyService.synthesizeData(audioData, OutputFormat.Mp3, parsedCMSdata.getMsid(), "Body");

					SlikeParameterDto parameterDto = PollyUtil.slikeCall(quickReadFeed, parsedCMSdata, property.AUDIOPATH, property.SLIKEURL);
					mongo.save(parameterDto, "ET_Polly");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@PostConstruct
	public void loadMsidList() {
		try{
			logger.info("sssssssssssssssssss");
			List<SlikeParameterDto> pollyList = (List<SlikeParameterDto>) mongo.getAllRecords("ET_Polly",new SlikeParameterDto());
			
			for (SlikeParameterDto slikeParameterDto : pollyList) {
				System.out.println("msid   "+slikeParameterDto.getMsid());
				Constants.uniqueMsids.add(slikeParameterDto.getMsid());
			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception while caching loadMsidList Cause  " + e.getMessage());
		}
		
		
	}
}