package com.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.Constants;
import com.util;
import com.Util.PollyUtil;
import com.amazonaws.services.polly.model.OutputFormat;
import com.google.gson.JsonObject;
import com.model.QuickReadBody;
import com.model.QuickReadFeed;
import com.service.CmsService;
import com.service.PollyService;
import com.til.et.polly.config.JdbcProperty;
import com.til.et.polly.dao.PollyDataService;


@RestController
public class PollyController {
	
	private static Logger logger = LogManager.getLogger(PollyController.class);

	@Autowired
	PollyService pollyService;
	
	@Autowired
	CmsService cmsService;
	
	@Autowired
	PollyDataService pollyDataService;
	
	@Autowired
	JdbcProperty property;
	
	//@Autowired
	public void LoadInitdata()
	{
		pollyService.initialize();
		System.out.println("init");
	}

	@RequestMapping(path = "/audio", produces = "audio/mpeg3")
	public @ResponseBody byte[] textToSpeech(@RequestParam("msg") String msg) throws IOException {

		//pollyService.initialize();

		// get the audio stream
		InputStream speechStream = pollyService.synthesize(msg, OutputFormat.Mp3);
		
		byte[] file = StreamUtils.copyToByteArray(speechStream);
		
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream("/usr/polly/testing.mpeg3"));
		stream.write(file);
		stream.close(); 
				
		return StreamUtils.copyToByteArray(speechStream);
	}
	
		
	@RequestMapping("/getCMSData")
	@ResponseBody
	public String StatusCheck(
			@RequestParam(value = "outputtype", required = false) String outputType,
			@RequestParam(value = "callback", required = false) String callBack) {
		String output = null;
		
		List<QuickReadFeed> QuickReadFeedList = cmsService.cmsDataList(property.QUICKPLIST);
		
		for (QuickReadFeed quickReadFeed : QuickReadFeedList) {
			try {
				if(!Constants.uniqueMsids.contains(quickReadFeed.getId())) {
				Constants.uniqueMsids.add(quickReadFeed.getId());
				System.out.println("quickReadFeed   "+quickReadFeed.getH1());
				logger.info("uniqueMsids  "+quickReadFeed.getId());
	   			//pollyService.synthesizeData(quickReadFeed.getH1(),  OutputFormat.Mp3, quickReadFeed.getId(),"Header");
				String bodyURL = quickReadFeed.getWu();
				bodyURL = bodyURL.replace("https://economictimes.indiatimes.com", "http://www1.economictimes.indiatimes.com/xml");
				QuickReadBody bodyText = util.parseData(bodyURL);		
				pollyService.synthesizeData(bodyText.getBodyText(),  OutputFormat.Mp3, bodyText.getMsid(),"Body");
				
				PollyUtil.slikeCall(quickReadFeed, bodyText, property.AUDIOPATH, property.SLIKEURL);	
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return output;
	}
	
	
	@RequestMapping("/slikestatus")
	public String SlikeStatus(
			@RequestParam(value = "outputtype", required = false) String outputType,
			@RequestParam(value = "msid", required = false) String msid,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "callback", required = false) String callBack) {
		String output = "";

		output = pollyDataService.slikeData(msid, status);
		JsonObject jsonobj = new JsonObject();
		jsonobj.addProperty("Response", output);
		
		return jsonobj.toString();
	}
	
	
	@RequestMapping("/dmkchangedmsids")
	public String Denmarkmsids(
			@RequestParam(value = "outputtype", required = false) String outputType,
			@RequestParam(value = "msid", required = false) String[] msids,
			@RequestParam(value = "callback", required = false) String callBack) {
		String output = "";

		output = pollyDataService.denmarkChangedMsids(msids);
		JsonObject jsonobj = new JsonObject();
		jsonobj.addProperty("Response", output);

		return jsonobj.toString();
	}
	
}