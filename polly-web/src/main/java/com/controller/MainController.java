package com.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

import com.til.et.mongo.config.EnableMongoSupport;
import com.til.et.polly.config.EnableJdbcSupport;



@ComponentScan(basePackages = { "com" })
@SpringBootApplication
@EnableScheduling
@EnableJdbcSupport
@EnableMongoSupport
@Controller
public class MainController extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MainController.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MainController.class, args);
	}
}