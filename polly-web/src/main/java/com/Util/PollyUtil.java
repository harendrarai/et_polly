package com.Util;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.model.QuickReadBody;
import com.model.QuickReadFeed;
import com.til.et.polly.dto.SlikeParameterDto;
import com.til.et.polly.dto.host_section;


public class PollyUtil {

	public static SlikeParameterDto slikeCall(QuickReadFeed quickReadFeed, QuickReadBody parsedCMSdata, String audioPath, String slikeURL) throws Exception {
		SlikeParameterDto parameterDto = new SlikeParameterDto();
		parameterDto.setTitle(parsedCMSdata.getTitle());
		parameterDto.setMsid(parsedCMSdata.getMsid());
		parameterDto.setGuid(Math.abs(parsedCMSdata.getBodyText().hashCode()));
		parameterDto.setTags("2.0");
		parameterDto.setType("audio");
		parameterDto.setSynopsis(quickReadFeed.getSyn());
		parameterDto.setDescription(parsedCMSdata.getBodyText());
		parameterDto.setUrl(audioPath + parsedCMSdata.getMsid() + ".mp3");
		parameterDto.setAutopublish("1");
		parameterDto.setPushtodenmark("1");
		parameterDto.setAuagencyid("");
		if(StringUtils.isNotBlank(parsedCMSdata.getAuagencyname())){
			parameterDto.setAuagencyname(parsedCMSdata.getAuagencyname());
			parameterDto.setReason("NA");
		}
		else {
			parameterDto.setAuagencyname("Others");
			parameterDto.setReason("Original article does not contain agency name");
			
		}
		
		Gson jsonobj = new Gson();		
		List<host_section> hostSection = new ArrayList<host_section>();
		
		host_section hostSectionObj = new host_section();
		hostSectionObj.setHost("153");
		List<String> sections = new ArrayList<String>();
		sections.add("67529778");
		//sections.add("");		
		hostSectionObj.setSection(sections);
		hostSection.add(hostSectionObj);
		
		/*host_section hostSectionObj2 = new host_section();
		hostSectionObj2.setHost("");
		List<String> sections2 = new ArrayList<String>();
		sections2.add("");
		sections2.add("");		
		hostSectionObj2.setSection(sections2);		
		hostSection.add(hostSectionObj2);*/
		
		parameterDto.setHost_section(hostSection);
		String jsonInString = jsonobj.toJson(parameterDto);
		System.out.println("jsonInString  "+jsonInString);
		
		String response = sendRestTemPlate(
				//"http://172.29.123.117/api.json?service=media&action=add&token=api-YLYqOKYQNM4vat5iOcZiQkoKNM4vat5iOcZiQkoKq04Qat5K3kViQMb-Q0U-3kRmm",
				slikeURL,
				jsonInString);
		
		parameterDto.setSlikeResponse(response);
		parameterDto.setCreatedDate(new Date(Calendar.getInstance().getTimeInMillis()));

		return parameterDto;
	}
	
	
	public static String sendRestTemPlate(String URL, String parameterDto) {
		
		String response = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
  			HttpEntity<String> entity = new HttpEntity<String>(parameterDto, headers);	
  			System.out.println("URL:::::::"+URL);
  			System.out.println("entity:::::::"+entity);
			ResponseEntity<String> loginResponse = restTemplate.exchange(URL, HttpMethod.POST, entity, String.class);
			if (loginResponse.getStatusCode() == HttpStatus.OK) {
				 response = loginResponse.getBody();
				 System.out.println("response  "+response);
			} else if (loginResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				 HttpStatus sss = loginResponse.getStatusCode();
				 response = sss.toString();
				 System.out.println("response  "+sss);
			} else {
				 HttpStatus httpRes = loginResponse.getStatusCode();
				 response = httpRes.toString();
				 System.out.println("response  "+httpRes);
			}
		} catch (RestClientException e) {
			e.printStackTrace();
		} catch (Exception ee) {
			ee.printStackTrace();
		}	
				
		return response;		
	}

}