package com.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickReadFeed implements Serializable{
	
	private static final long serialVersionUID = -2263549056037200680L;
	private String hl;
	private String da;
	private String id;
	private String syn;
	private String wu;
	private String ga;
	private String im;
	private String sv;
	
	public String getH1() {
		return hl;
	}
	public void setH1(String hl) {
		this.hl = hl;
	}
	public String getDa() {
		return da;
	}
	public void setDa(String da) {
		this.da = da;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSyn() {
		return syn;
	}
	public void setSyn(String syn) {
		this.syn = syn;
	}
	public String getWu() {
		return wu;
	}
	public void setWu(String wu) {
		this.wu = wu;
	}
	public String getGa() {
		return ga;
	}
	public void setGa(String ga) {
		this.ga = ga;
	}	
	public String getIm() {
		return im;
	}
	public void setIm(String im) {
		this.im = im;
	}
	public String getSv() {
		return sv;
	}
	public void setSv(String sv) {
		this.sv = sv;
	}
	@Override
	public String toString() {
		return "QuickReadFeed [h1=" + hl + ", da=" + da + ", id=" + id + ", syn=" + syn + ", wu=" + wu + ", ga=" + ga
				+ ", im=" + im + ", sv=" + sv + "]";
	}
		
	
}