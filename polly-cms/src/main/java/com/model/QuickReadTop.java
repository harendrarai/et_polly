package com.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuickReadTop  implements Serializable{

	private static final long serialVersionUID = 7290705406577651225L;
	//private String pn;
	//private String ibeat;
	private List<QuickReadFeed> NewsItem;
	
	/*public String getPn() {
		return pn;
	}
	public void setPn(String pn) {
		this.pn = pn;
	}
	public String getIbeat() {
		return ibeat;
	}
	public void setIbeat(String ibeat) {
		this.ibeat = ibeat;
	}*/
	public List<QuickReadFeed> getNewsItem() {
		return NewsItem;
	}
	@Override
	public String toString() {
		return "QuickReadTop [NewsItem=" + NewsItem + "]";
	}
	public void setNewsItem(List<QuickReadFeed> newsItem) {
		NewsItem = newsItem;
	}
		
}
