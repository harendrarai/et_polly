package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "NewsML")
@XmlAccessorType (XmlAccessType.FIELD)
public class NewDataSetDTO {
	
	@XmlElement(name = "NewsItem")
	private List<CMSmorningFeed> cMSmorningFeed = new ArrayList<CMSmorningFeed>(0);
	
	@XmlElement(name = "pn")
	private CMSTitle CMStitle = new CMSTitle();

	public List<CMSmorningFeed> getcMSmorningFeed() {
		return cMSmorningFeed;
	}

	public void setcMSmorningFeed(List<CMSmorningFeed> cMSmorningFeed) {
		this.cMSmorningFeed = cMSmorningFeed;
	}

	public CMSTitle getCMStitle() {
		return CMStitle;
	}

	public void setCMStitle(CMSTitle cMStitle) {
		CMStitle = cMStitle;
	}

	@Override
	public String toString() {
		return "NewDataSetDTO [cMSmorningFeed=" + cMSmorningFeed + ", CMStitle=" + CMStitle + "]";
	}

}