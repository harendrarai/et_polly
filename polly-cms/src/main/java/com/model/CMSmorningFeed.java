package com.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "NewsItem")
@XmlAccessorType (XmlAccessType.FIELD)
public class CMSmorningFeed  implements Serializable {
	private static final long serialVersionUID = 617299082117293110L;
	
	@XmlElement(name= "h1")
	private String heading;
	
	@XmlElement(name= "da")
	private String date;
	
	@XmlElement(name= "id")
	private String cmsid;
	
	@XmlElement(name= "syn")
	private String news;
	
	@XmlElement(name= "wu")
	private String url;
	
	@XmlElement(name= "ga")
	private String ga;
	
	@XmlElement(name= "im")
	private String image;
	
	@XmlElement(name= "sv")
	private String type;
	
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCmsid() {
		return cmsid;
	}
	public void setCmsid(String cmsid) {
		this.cmsid = cmsid;
	}
	public String getNews() {
		return news;
	}
	public void setNews(String news) {
		this.news = news;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getGa() {
		return ga;
	}
	public void setGa(String ga) {
		this.ga = ga;
	}	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "CMSmorningFeed [heading=" + heading + ", date=" + date + ", cmsid=" + cmsid + ", news=" + news
				+ ", url=" + url + ", ga=" + ga + ", image=" + image + ", type=" + type + ", getHeading()="
				+ getHeading() + ", getDate()=" + getDate() + ", getCmsid()=" + getCmsid() + ", getNews()=" + getNews()
				+ ", getUrl()=" + getUrl() + ", getGa()=" + getGa() + ", getImage()=" + getImage() + ", getType()="
				+ getType() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	
}