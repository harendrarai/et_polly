package com.model;

public class QuickReadBody {
	
	private String date;
	private String msid;
	private String title;
	private String bodyText;
	private String auagencyid;
	private String auagencyname;
	private String keywords;
	private String image;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMsid() {
		return msid;
	}
	public void setMsid(String msid) {
		this.msid = msid;
	}
	public String getBodyText() {
		return bodyText;
	}
	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}	
	public String getAuagencyid() {
		return auagencyid;
	}
	public void setAuagencyid(String auagencyid) {
		this.auagencyid = auagencyid;
	}
	public String getAuagencyname() {
		return auagencyname;
	}
	public void setAuagencyname(String auagencyname) {
		this.auagencyname = auagencyname;
	}	
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return "QuickReadBody [date=" + date + ", msid=" + msid + ", title=" + title + ", bodyText=" + bodyText
				+ ", auagencyid=" + auagencyid + ", auagencyname=" + auagencyname + ", keywords=" + keywords
				+ ", image=" + image + "]";
	}
	

}