package com.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pn")
@XmlAccessorType (XmlAccessType.FIELD)
public class CMSTitle  implements Serializable {
	
	private static final long serialVersionUID = 8866596765206931149L;
	
	@XmlElement(name= "tp")
	private String tp;
	
	@XmlElement(name= "pno")
	private String pno;
	
	@XmlElement(name= "tts")
	private String tts;
	
	public String getTp() {
		return tp;
	}
	public void setTp(String tp) {
		this.tp = tp;
	}
	public String getPno() {
		return pno;
	}
	public void setPno(String pno) {
		this.pno = pno;
	}
	public String getTts() {
		return tts;
	}
	public void setTts(String tts) {
		this.tts = tts;
	}
	
	@Override
	public String toString() {
		return "CMSTitle [tp=" + tp + ", pno=" + pno + ", tts=" + tts + "]";
	}
	
}