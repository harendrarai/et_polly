package com.service;

import java.util.List;

import com.model.CMSmorningFeed;
import com.model.QuickReadFeed;

public interface CmsService {
	
	public List<CMSmorningFeed> cmsData(String cmsURL,String type);
	
	public List<QuickReadFeed> cmsDataList(String cmsURL);
}
