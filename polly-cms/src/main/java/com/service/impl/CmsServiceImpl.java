package com.service.impl;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.util;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.model.CMSTitle;
import com.model.CMSmorningFeed;
import com.model.NewDataSetDTO;
import com.model.QuickReadBody;
import com.model.QuickReadFeed;
import com.model.QuickReadTop;
import com.service.CmsService;

@Service
public class CmsServiceImpl implements CmsService {
	
	private static Logger LOG =LoggerFactory.getLogger("CmsServiceImpl");

	@Override
	public List<CMSmorningFeed> cmsData(String cmsURL, String type) {
		List<CMSmorningFeed> CMSmorningFeedList = null;
		try {
			String Response = util.URLData(cmsURL);
			System.out.println("Response   " + Response);
			NewDataSetDTO cmsdataDto = (NewDataSetDTO) JAXBContext.newInstance(NewDataSetDTO.class).createUnmarshaller()
					.unmarshal(new StringReader(Response));
			CMSmorningFeedList = cmsdataDto.getcMSmorningFeed();
			CMSTitle cmsTitle = cmsdataDto.getCMStitle();
			System.out.println("cmsTitle   " + cmsTitle.getTts());
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		System.out.println("CMSmorningFeedList  " + CMSmorningFeedList.size());
		for (CMSmorningFeed cmSmorningFeed : CMSmorningFeedList) {
			System.out.println("cmSmorningFeed   " + cmSmorningFeed.getHeading());
		}
		return CMSmorningFeedList;

	}

	@Override
	public List<QuickReadFeed> cmsDataList(String cmsURL) {
		List<QuickReadFeed> QuickReadFeedList = null;
		try {
			String json = util.URLData(cmsURL);
			System.out.println("Response   "+json);
			Gson gson = new Gson();
			QuickReadTop quickReadTop = gson.fromJson(json, QuickReadTop.class); 
			
			System.out.println("quickReadTop   "+quickReadTop);
			QuickReadFeedList = quickReadTop.getNewsItem();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("QuickReadFeedList  "+QuickReadFeedList.size());
		/*for (QuickReadFeed quickReadFeed : QuickReadFeedList) {
			System.out.println("quickReadFeed   "+quickReadFeed.getH1());
			String bodyURL = quickReadFeed.getWu();
			bodyURL = bodyURL.replace("https://economictimes.indiatimes.com", "http://www1.economictimes.indiatimes.com/xml");
			QuickReadBody bodyText = util.parseData(bodyURL);
			
		}*/
		return QuickReadFeedList;
	}

	
}