package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.model.QuickReadBody;

public class util {
	
	private static Logger logger = LogManager.getLogger(util.class);

	public static String URLData(String URL) {
		String output = "";
		try {
			URL url;
			url = new URL(URL);
			URLConnection con = url.openConnection();
			con.setConnectTimeout(120000);
			con.setReadTimeout(120000);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				output = output + inputLine;
			}
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}
	
	
	public static QuickReadBody parseData(String url) {
		Document doc;
		QuickReadBody parseData = new QuickReadBody();
		try {
			doc = Jsoup.connect(url).get();
			for (Element e : doc.getElementsByTag("tdate")) {
				parseData.setDate(e.text());
			}
			for (Element e : doc.getElementsByTag("article")) {
				Elements e1 = e.getElementsByTag("msid");
				parseData.setMsid(e1.get(0).text());
				
				Elements e2 = e.getElementsByTag("artag");
				parseData.setAuagencyname(e2.get(0).text());
				
				Elements e3 = e.getElementsByTag("arttextnew");
				String textData = e3.text();
				
				for(Element e4 : e3)
				{
					Elements e6 = e4.getElementsByAttribute("data-msid");
					if (e6 != null) {
						for (Element element : e6) {
							String replaceData = element.text();
							textData = textData.replace(replaceData, "");
						}
					}
				}
				
				parseData.setBodyText(textData.trim());
				
				Elements e4 = e.getElementsByTag("arttitle");
				String title = e4.text();
				
				Elements e6 = e.getElementsByTag("embedarticle ");
				if (e6 != null) {
					for (Element element : e6) {
						String replaceData = element.getElementsByTag("arttitle").text();
						title = title.replace(replaceData, "");
					}
				}
				
				parseData.setTitle(title.trim());
			}	
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return parseData;
	}
	
	public static void main(String args[]){
		parseData("http://www1.economictimes.indiatimes.com/xml/small-biz/startups/newsbuzz/delivery-hero-buys-zomatos-uae-unit/articleshow/68263500.cms");
	}
	
}