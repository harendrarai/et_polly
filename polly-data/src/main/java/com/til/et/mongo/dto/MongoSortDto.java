package com.til.et.mongo.dto;

public class MongoSortDto {

	public enum sortType {
		desc, asc;
	}

	private String field;

	private sortType type;
	
	public MongoSortDto() {
		super();
	}
	
	public MongoSortDto(String field, sortType type) {
		super();
		this.field = field;
		this.type = type;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public sortType getType() {
		return type;
	}

	public void setType(sortType type) {
		this.type = type;
	}

}
