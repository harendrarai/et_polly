package com.til.et.mongo.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.til.et.polly.config.JdbcProperty;

@Configuration
public class MongoConfig {
	
	private static Logger LOG =LoggerFactory.getLogger("MongoConfig");
	
	@Autowired
	JdbcProperty property;

	@SuppressWarnings("serial")
	public MongoClient mongoClient() {

		System.out.println("**** Mongo loaded Successfully ****");
		List<ServerAddress> serverList = new ArrayList<ServerAddress>();

		String[] serverString = property.mongoHost.split(",");
		for (String host : serverString) {
			serverList.add(new ServerAddress(host));
		}

		return new MongoClient(serverList, new ArrayList<MongoCredential>() {

			{
				add(MongoCredential.createCredential(property.mongoUserName, property.mongoDb,
						property.mongoPassword.toCharArray()));
			}
		});
	}

	public MongoDbFactory mongoDbFactory() throws Exception { LOG.info("Mongo db");
		return new SimpleMongoDbFactory(mongoClient(), property.mongoDb);
	}

	@Bean
	@Primary
	public MongoTemplate mongoTemplate() throws Exception {
		MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
		return mongoTemplate;
	}

}
