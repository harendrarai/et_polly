package com.til.et.mongo.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.til.et.mongo.service.impl.MongoServiceImpl;
import com.til.et.polly.config.JdbcProperty;

@Configuration
@Retention(RetentionPolicy.RUNTIME)
@Import({ MongoConfig.class, MongoServiceImpl.class, JdbcProperty.class })
public @interface EnableMongoSupport {

}
