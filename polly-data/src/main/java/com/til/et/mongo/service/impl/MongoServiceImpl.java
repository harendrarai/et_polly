package com.til.et.mongo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.DBCursor;
import com.mongodb.util.JSON;
import com.til.et.mongo.dto.MongoConditionDto;
import com.til.et.mongo.dto.MongoPageSummaryDto;
import com.til.et.mongo.dto.MongoQueryDto;
import com.til.et.mongo.service.MongoService;

@Repository
public class MongoServiceImpl implements MongoService {

	private static Integer MONGO_DEFAULT_PAGE_SIZE = 10;

	private static Integer MONGO_DEFAULT_PAGE_NUMBER = 1;

	@Autowired
	MongoTemplate mongo;

	public void save(Object obj) {

		mongo.save(obj);
	}

	public void saveAll(List<?> obj) {

		mongo.insertAll(obj);
	}

	public void update(Object obj) {

		mongo.save(obj);

	}

	public void updateAll(List<?> obj) {

		for (Object object : obj) {

			mongo.save(object);
		}

	}

	public void save(Object obj, String collectionName) {

		mongo.save(obj, collectionName);
	}

	public void saveAll(List<?> obj, String collectionName) {

		mongo.insert(obj, collectionName);
	}

	public void update(Object obj, String collectionName) {

		mongo.save(obj, collectionName);

	}

	public void updateAll(List<?> obj, String collectionName) {

		for (Object object : obj) {

			mongo.save(object, collectionName);
		}

	}

	@SuppressWarnings("rawtypes")
	public int countRecords(MongoQueryDto mongoQuery, Class classObj) {

		Query query = new Query();

		processQuery(query, mongoQuery);

		return (int) mongo.count(query, classObj);
	}

	@SuppressWarnings("rawtypes")
	public List<?> getRecords(MongoQueryDto mongoQuery, Class classObj, Boolean getJsonObject) {

		return getRecords(mongoQuery, mongo.getCollectionName(classObj), getJsonObject);
	}

	public List<?> getRecords(MongoQueryDto mongoQuery, String collectionName, Boolean getJsonObject) {

		Query query = new Query();

		processQuery(query, mongoQuery);

		DBCursor cur = mongo.getCollection(collectionName).find(query.getQueryObject(), query.getFieldsObject())
				.sort(query.getSortObject()).limit(query.getLimit()).skip(query.getSkip());

		if (getJsonObject) {
			List<JSONObject> list = new ArrayList<>();
			while (cur.hasNext()) {

				JSONObject obj = null;
				try {
					obj = new JSONObject(JSON.serialize(cur.next()));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				obj.remove("_id");
				if (obj.has("_class")) {
					obj.remove("_class");
				}

				list.add(obj);
			}

			return list;

		} else {
			List<Object> list = new ArrayList<>();
			while (cur.hasNext()) {

				Object obj = cur.next();
				list.add(obj);
			}

			return list;
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<?> getRecords(MongoQueryDto mongoQuery, Class classObj) {

		Query query = new Query();

		processQuery(query, mongoQuery);

		return mongo.find(query, classObj);
	}

	private void processQuery(Query query, MongoQueryDto mongoQuery) {

		List<MongoConditionDto> conditions = mongoQuery.getConditions();

		if (conditions != null) {
			for (MongoConditionDto mongoConditionDto : conditions) {

				query.addCriteria(processQueryCriteria(mongoConditionDto));
			}
		}

		List<String> fields = mongoQuery.getFields();

		if (fields != null && !fields.isEmpty()) {
			for (String field : fields) {
				query.fields().include(field);
			}
		} else {
			// query.fields().exclude("id");
		}

		if (mongoQuery.getPageSize() != null) {

			if (mongoQuery.getPageSize() > 0) {
				query.limit(mongoQuery.getPageSize());
			}

		} else {
			query.limit(MONGO_DEFAULT_PAGE_SIZE);
		}

		if (mongoQuery.getPageNumber() != null) {

			if (mongoQuery.getPageNumber() == 0) {
				mongoQuery.setPageNumber(1);
			}

			int skipValue = query.getLimit() * (mongoQuery.getPageNumber() - 1);

			query.skip(skipValue);
		} else {
			int skipValue = query.getLimit() * (MONGO_DEFAULT_PAGE_NUMBER - 1);

			query.skip(skipValue);

		}

		if (mongoQuery.getStart() != null) {
			query.skip(mongoQuery.getStart());
		}

		if (mongoQuery.getLimit() != null) {
			query.limit(mongoQuery.getLimit());
		}

		if (mongoQuery.getSorting() != null) {

			switch (mongoQuery.getSorting().getType()) {
			case desc:
				query.with(new Sort(Sort.Direction.DESC, mongoQuery.getSorting().getField()));
				break;
			case asc:
				query.with(new Sort(Sort.Direction.ASC, mongoQuery.getSorting().getField()));
				break;
			}
		}

		if (mongoQuery.isDebug()) {
			System.out.println("mongo query::" + query.toString());
		}

	}

	@SuppressWarnings({ "unchecked" })
	private Criteria processQueryCriteria(MongoConditionDto mongoConditionDto) {

		Criteria c = new Criteria();
		if (mongoConditionDto.getOrCondition() == null && mongoConditionDto.getAndCondition() == null) {
			switch (mongoConditionDto.getOperation()) {
			case equal:
				c = Criteria.where(mongoConditionDto.getFieldName()).is(mongoConditionDto.getValue());
				break;
			case in:
				c = Criteria.where(mongoConditionDto.getFieldName()).in((List<Object>) mongoConditionDto.getValue());
				break;
			case greaterThan:
				c = Criteria.where(mongoConditionDto.getFieldName()).gt(mongoConditionDto.getValue());
				break;
			case lessThan:
				c = Criteria.where(mongoConditionDto.getFieldName()).lt(mongoConditionDto.getValue());
				break;
			case greaterThanEq:
				c = Criteria.where(mongoConditionDto.getFieldName()).gte(mongoConditionDto.getValue());
				break;
			case lessThanEq:
				c = Criteria.where(mongoConditionDto.getFieldName()).lte(mongoConditionDto.getValue());
				break;
			case notEqual:
				c = Criteria.where(mongoConditionDto.getFieldName()).ne(mongoConditionDto.getValue());
				break;
			}
			return c;
		}

		if (mongoConditionDto.getAndCondition() == null) {
			return c.orOperator(
					processQueryCriteria(new MongoConditionDto(mongoConditionDto.getFieldName(),
							mongoConditionDto.getValue(), mongoConditionDto.getOperation())),
					processQueryCriteria(mongoConditionDto.getOrCondition()));
		} else {
			return c.andOperator(
					processQueryCriteria(new MongoConditionDto(mongoConditionDto.getFieldName(),
							mongoConditionDto.getValue(), mongoConditionDto.getOperation())),
					processQueryCriteria(mongoConditionDto.getAndCondition()));
		}

	}

	@SuppressWarnings({ "rawtypes" })
	public MongoPageSummaryDto getPageSummary(MongoQueryDto mongoQuery, Class classObj) {

		MongoPageSummaryDto pageSummary = new MongoPageSummaryDto();

		int records = countRecords(mongoQuery, classObj);

		if (mongoQuery.getPageSize() == null) {
			mongoQuery.setPageSize(MONGO_DEFAULT_PAGE_SIZE);
		}

		if (mongoQuery.getPageNumber() == null) {
			mongoQuery.setPageNumber(MONGO_DEFAULT_PAGE_NUMBER);
		}

		pageSummary.setTotalRecords(records);
		int totalPages = (int) Math.ceil((float) records / mongoQuery.getPageSize());
		pageSummary.setTotalPages(totalPages);
		pageSummary.setPageSize(mongoQuery.getPageSize());
		pageSummary.setPageNumber(mongoQuery.getPageNumber());

		return pageSummary;

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getRecords(MongoQueryDto mongoQuery, String collectionName, T dto) {

		Query query = new Query();

		processQuery(query, mongoQuery);

		DBCursor cur = mongo.getCollection(collectionName).find(query.getQueryObject(), query.getFieldsObject())
				.sort(query.getSortObject()).limit(query.getLimit()).skip(query.getSkip());

		List<T> list = new ArrayList<>();
		while (cur.hasNext()) {

			list.add((T) mongo.getConverter().read(dto.getClass(), cur.next()));
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getAllRecords(String collectionName, T dto) {

		DBCursor cur = mongo.getCollection(collectionName).find();

		List<T> list = new ArrayList<>();
		while (cur.hasNext()) {

			list.add((T) mongo.getConverter().read(dto.getClass(), cur.next()));
		}
		return list;
	}
}