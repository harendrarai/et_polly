package com.til.et.mongo.dto;

public class MongoConditionDto {

	public enum Operation {
		equal, greaterThan, lessThan, in, greaterThanEq, lessThanEq, notEqual;
	}

	private String fieldName;

	private Object value;

	private Operation operation;

	private MongoConditionDto orCondition;
	
	private MongoConditionDto andCondition;

	public MongoConditionDto() {
		super();
	}

	public MongoConditionDto(String fieldName, Object value, Operation operation) {
		this.fieldName = fieldName;
		this.value = value;
		this.operation = operation;
	}

	public MongoConditionDto(String fieldName, Object value, Operation operation, MongoConditionDto orCondition) {
		super();
		this.fieldName = fieldName;
		this.value = value;
		this.operation = operation;
		this.orCondition = orCondition;
	}
	
	public MongoConditionDto(String fieldName, Object value, Operation operation, MongoConditionDto andCondition, int andFlag) {
		super();
		this.fieldName = fieldName;
		this.value = value;
		this.operation = operation;
		this.andCondition = andCondition;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public MongoConditionDto getOrCondition() {
		return orCondition;
	}

	public void setOrCondition(MongoConditionDto orCondition) {
		this.orCondition = orCondition;
	}

	public MongoConditionDto getAndCondition() {
		return andCondition;
	}

	public void setAndCondition(MongoConditionDto andCondition) {
		this.andCondition = andCondition;
	}

	
	
	
}
