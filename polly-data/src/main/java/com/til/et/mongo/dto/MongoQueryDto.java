package com.til.et.mongo.dto;

import java.util.List;

public class MongoQueryDto {

	private List<String> fields;

	private List<MongoConditionDto> conditions;

	private MongoSortDto sorting;

	private Integer start;

	private Integer limit;

	private Integer pageNumber;

	private Integer pageSize;

	private boolean debug;

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public List<MongoConditionDto> getConditions() {
		return conditions;
	}

	public void setConditions(List<MongoConditionDto> conditions) {
		this.conditions = conditions;
	}

	public MongoSortDto getSorting() {
		return sorting;
	}

	public void setSorting(MongoSortDto sorting) {
		this.sorting = sorting;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

}
