package com.til.et.mongo.service;

import java.util.List;

import com.til.et.mongo.dto.MongoPageSummaryDto;
import com.til.et.mongo.dto.MongoQueryDto;

public interface MongoService {

	public void save(Object obj);

	public void saveAll(List<?> obj);

	public void save(Object obj, String collectionName);

	public void saveAll(List<?> obj, String collectionName);

	public void update(Object obj);

	public void updateAll(List<?> obj);

	public void update(Object obj, String collectionName);

	public void updateAll(List<?> obj, String collectionName);

	public List<?> getRecords(MongoQueryDto mongoQuery, @SuppressWarnings("rawtypes") Class classObj);

	public List<?> getRecords(MongoQueryDto query, Class<?> class1, Boolean getJsonObject);

	public List<?> getRecords(MongoQueryDto mongoQuery, String collectionName, Boolean getJsonObject);
	
	public <T> List<T> getRecords(MongoQueryDto mongoQuery, String collectionName, T dto);

	public int countRecords(MongoQueryDto mongoQuery, @SuppressWarnings("rawtypes") Class classObj);

	public MongoPageSummaryDto getPageSummary(MongoQueryDto mongoQuery, @SuppressWarnings("rawtypes") Class classObj);

	public <T> List<T> getAllRecords(String collectionName, T dto);
}
