package com.til.et.polly.dto;

import java.util.List;

public class host_section {

	private String host;
	private List<String> section;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public List<String> getSection() {
		return section;
	}
	public void setSection(List<String> section) {
		this.section = section;
	}
	
	
	
}
