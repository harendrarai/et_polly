package com.til.et.polly.impl;

import java.sql.Types;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.til.et.polly.dao.PollyDataService;

@Repository
public class PollyDataServiceImpl implements PollyDataService{
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String slikeData(String msid, String status) {
		String message = "";
		try{
			String query = "INSERT INTO Polly_SlikeStatus(MSID,Status) values(?,?)";
			Object[] params = new Object[] { msid, status};
            int[] types = new int[] { Types.VARCHAR, Types.VARCHAR };
            jdbcTemplate.update(query, params, types);
            message = "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			message = "ERROR";
		}		
		return message;
	}

	@Override
	public String denmarkChangedMsids(String[] msids) {
		String message = "";
		try{
			String msidsN ="";
			for (String msid : msids) {
				msidsN += msid +"," ;
			}			
			msidsN= msidsN.substring(0, msidsN.length()-1);
			
			String query = "INSERT INTO Polly_DenmarkData(msids) values(?)";
			Object[] params = new Object[] {msidsN};
            int[] types = new int[] { Types.VARCHAR };
            jdbcTemplate.update(query, params, types);
            message = "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			message = "ERROR";
		}		
		return message;
	}

}