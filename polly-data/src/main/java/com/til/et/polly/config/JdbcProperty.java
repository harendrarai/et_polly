package com.til.et.polly.config;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("file:/usr/ET_POLLY_CMS/system.properties")
public class JdbcProperty {

	@Resource
	private Environment env;

	@Value("${mySql.host:null}")
	public String mysqlHost;

	@Value("${mySql.db:null}")
	public String mysqlDb;

	@Value("${mySql.port:null}")
	public Integer mysqlPort;

	@Value("${mySql.username:null}")
	public String mysqlUserName;

	@Value("${mySql.password:null}")
	public String mysqlPassword;
	
	@Value("${Mongo.host:null}")
	public String mongoHost;

	@Value("${Mongo.db:null}")
	public String mongoDb;

	@Value("${Mongo.port:null}")
	public Integer port;

	@Value("${Mongo.username:null}")
	public String mongoUserName;

	@Value("${Mongo.password:null}")
	public String mongoPassword;
	
	
	@Value("${QUICK_PLIST:null}")
	public String QUICKPLIST;
	@Value("${MORNING_PLIST:null}")
	public String MORNINGPLIST;
	@Value("${TOPNEWS_PLIST:null}")
	public String TOPNEWSPLIST;
	
	@Value("${AUDIO_PATH:null}")
	public String AUDIOPATH;
	@Value("${SLIKE_URL:null}")
	public String SLIKEURL;
	
}