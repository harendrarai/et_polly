package com.til.et.polly.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class JdbcConfig {

	@Autowired
	JdbcProperty property;

	@Bean
	public DriverManagerDataSource dataSourceMySql() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setPassword(property.mysqlPassword);
		String url = "jdbc:mysql://" + property.mysqlHost + ":" + property.mysqlPort + "/" + property.mysqlDb;
		System.out.println("MYSQL ConnectionURL is :: " + url);
		dataSource.setUrl(url);
		dataSource.setUsername(property.mysqlUserName);
		return dataSource;
	}

	@Bean(name = "jdbcTemplateMysql")
	public JdbcTemplate jdbcTemplateMysql() {
		System.out.println("**** Jdbc MYSQL loaded Successfully ****");
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSourceMySql());
		return jdbcTemplate;
	}

}
