package com.til.et.polly.dto;

import java.util.Date;
import java.util.List;

public class SlikeParameterDto {
	
	private String title;
	private String synopsis;
	private String description;
	private String tags;
	private String url;
	private String type;
	private String msid;
	private Integer guid;
	private String autopublish;
	private String pushtodenmark;
	private String auagencyid;
	private String auagencyname;
	private String reason;
	
	private List<host_section> host_section;
	
	private String slikeResponse;
	private Date createdDate;
	
	public String getSlikeResponse() {
		return slikeResponse;
	}
	public void setSlikeResponse(String slikeResponse) {
		this.slikeResponse = slikeResponse;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMsid() {
		return msid;
	}
	public void setMsid(String msid) {
		this.msid = msid;
	}
	public Integer getGuid() {
		return guid;
	}
	public void setGuid(Integer guid) {
		this.guid = guid;
	}
	public String getAutopublish() {
		return autopublish;
	}
	public void setAutopublish(String autopublish) {
		this.autopublish = autopublish;
	}
	public String getPushtodenmark() {
		return pushtodenmark;
	}
	public void setPushtodenmark(String pushtodenmark) {
		this.pushtodenmark = pushtodenmark;
	}
	public String getAuagencyid() {
		return auagencyid;
	}
	public void setAuagencyid(String auagencyid) {
		this.auagencyid = auagencyid;
	}
	public String getAuagencyname() {
		return auagencyname;
	}
	public void setAuagencyname(String auagencyname) {
		this.auagencyname = auagencyname;
	}
	public List<host_section> getHost_section() {
		return host_section;
	}
	public void setHost_section(List<host_section> host_section) {
		this.host_section = host_section;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
		
	
}