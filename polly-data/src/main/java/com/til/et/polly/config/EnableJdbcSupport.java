package com.til.et.polly.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Retention(RetentionPolicy.RUNTIME)
@Import({JdbcConfig.class, JdbcProperty.class})
public @interface EnableJdbcSupport {

	
	
}
