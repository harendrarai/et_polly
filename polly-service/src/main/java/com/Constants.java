package com;

import java.util.HashSet;
import java.util.Set;

import org.awaitility.Duration;

import com.amazonaws.services.polly.model.OutputFormat;

public class Constants {

	public static final int SYNTHESIS_TASK_TIMEOUT_SECONDS = 30000;
	public static final String OUTPUT_FORMAT_MP3 = OutputFormat.Mp3.toString();
	public static final String OUTPUT_BUCKET = "et-polly";
	public static final String SNS_TOPIC_ARN = "arn:aws:sns:ap-south-1:406154381445:speech";
	public static final Duration SYNTHESIS_TASK_POLL_INTERVAL = Duration.FIVE_SECONDS;
	public static final Duration SYNTHESIS_TASK_POLL_DELAY = Duration.TEN_SECONDS;
	public static  Set<String> uniqueMsids = new HashSet<String>();

}
