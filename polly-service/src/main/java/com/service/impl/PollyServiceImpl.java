package com.service.impl;

import static org.awaitility.Awaitility.await;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.Constants;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.polly.AmazonPolly;
import com.amazonaws.services.polly.AmazonPollyClient;
import com.amazonaws.services.polly.AmazonPollyClientBuilder;
import com.amazonaws.services.polly.model.GetSpeechSynthesisTaskRequest;
import com.amazonaws.services.polly.model.GetSpeechSynthesisTaskResult;
import com.amazonaws.services.polly.model.OutputFormat;
import com.amazonaws.services.polly.model.StartSpeechSynthesisTaskRequest;
import com.amazonaws.services.polly.model.StartSpeechSynthesisTaskResult;
import com.amazonaws.services.polly.model.SynthesisTask;
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest;
import com.amazonaws.services.polly.model.SynthesizeSpeechResult;
import com.amazonaws.services.polly.model.TaskStatus;
import com.amazonaws.services.polly.model.TextType;
import com.amazonaws.services.polly.model.Voice;
import com.amazonaws.services.polly.model.VoiceId;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.service.PollyService;

@Service
public class PollyServiceImpl implements PollyService {
	
	private static Logger logger = LogManager.getLogger(PollyServiceImpl.class);
	

	private AmazonPollyClient polly;
	public static AmazonPolly AMAZON_POLLY_CLIENT;
	private Voice voice;
	

	public void initialize() {
		try {
			
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
					"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
			AMAZON_POLLY_CLIENT = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
					.withRegion(Region.getRegion(Regions.AP_SOUTH_1).getName()).build();
			logger.info("AMAZON_POLLY_CLIENT Called");
			/*BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
					"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");

			polly = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
					.withRegion(Region.getRegion(Regions.US_EAST_2).getName()).build();

			DescribeVoicesRequest describeVoicesRequest = new DescribeVoicesRequest();
			DescribeVoicesResult describeVoicesResult = polly.describeVoices(describeVoicesRequest);
			voice = describeVoicesResult.getVoices().get(0);*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public InputStream synthesize(String text, OutputFormat format) {
		SynthesizeSpeechRequest synthReq = new SynthesizeSpeechRequest().withText(text).withVoiceId(VoiceId.Raveena)
				.withOutputFormat(format);
		
		System.out.println(synthReq);
		
		SynthesizeSpeechResult synthRes = polly.synthesizeSpeech(synthReq);

		return synthRes.getAudioStream();
	}

	
	public void synthesizeList(List<String> cmsDataList, OutputFormat format, String cmsid) {
		for (String cmsData : cmsDataList) {
			try {
				SynthesizeSpeechRequest synthReq = new SynthesizeSpeechRequest().withText(cmsData).withVoiceId(VoiceId.Raveena).withOutputFormat(format);
				System.out.println(synthReq);

				SynthesizeSpeechResult synthRes = polly.synthesizeSpeech(synthReq);
				InputStream speechStream = synthRes.getAudioStream();
				synthRes.setRequestCharacters(10000);
				byte[] file = StreamUtils.copyToByteArray(speechStream);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream("/usr/polly/" + cmsid + ".mpeg3"));
				stream.write(file);
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void synthesizeData(String cmsData, OutputFormat format, String cmsid, String filename) {
		try {
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
					"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
			AMAZON_POLLY_CLIENT = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
					.withRegion(Region.getRegion(Regions.AP_SOUTH_1).getName()).build();
		
			StartSpeechSynthesisTaskRequest request = new StartSpeechSynthesisTaskRequest()
					.withOutputFormat(Constants.OUTPUT_FORMAT_MP3)
					.withText(cmsData)
					.withTextType(TextType.Text).withVoiceId(VoiceId.Raveena).withOutputS3BucketName(Constants.OUTPUT_BUCKET)
					.withSnsTopicArn(Constants.SNS_TOPIC_ARN);

			StartSpeechSynthesisTaskResult result = AMAZON_POLLY_CLIENT.startSpeechSynthesisTask(request);
			String taskId = result.getSynthesisTask().getTaskId();
			System.out.println("Task id " + taskId);

			await().with().pollInterval(Constants.SYNTHESIS_TASK_POLL_INTERVAL).pollDelay(Constants.SYNTHESIS_TASK_POLL_DELAY)
					.atMost(Constants.SYNTHESIS_TASK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
					.until(() -> getSynthesisTaskStatus(taskId).equals(TaskStatus.Completed.toString()));

			System.out.println("Return Json  " + getSynthesisTask(taskId));
			
			PollyDownload(taskId,cmsid,filename);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	private SynthesisTask getSynthesisTask(String taskId) {
		GetSpeechSynthesisTaskRequest getSpeechSynthesisTaskRequest = new GetSpeechSynthesisTaskRequest()
				.withTaskId(taskId);
		GetSpeechSynthesisTaskResult result = AMAZON_POLLY_CLIENT.getSpeechSynthesisTask(getSpeechSynthesisTaskRequest);
		return result.getSynthesisTask();
	}

	private String getSynthesisTaskStatus(String taskId) {

		GetSpeechSynthesisTaskRequest getSpeechSynthesisTaskRequest = new GetSpeechSynthesisTaskRequest()
				.withTaskId(taskId);

		GetSpeechSynthesisTaskResult result = AMAZON_POLLY_CLIENT.getSpeechSynthesisTask(getSpeechSynthesisTaskRequest);
		return result.getSynthesisTask().getTaskStatus();
	}
	
	   
    
    /*public void synthesizeData1(String cmsData, OutputFormat format, String cmsid, String filename) {
				try {
				SynthesizeSpeechRequest synthReq = new SynthesizeSpeechRequest().withText(cmsData).withVoiceId(VoiceId.Raveena).withOutputFormat(format);
				System.out.println(synthReq);

				SynthesizeSpeechResult synthRes = polly.synthesizeSpeech(synthReq);
				InputStream speechStream = synthRes.getAudioStream();
				synthRes.setRequestCharacters(10000);
				byte[] file = StreamUtils.copyToByteArray(speechStream);
				String Path = "/usr/polly/" +filename+"_"+ cmsid + ".mpeg3";
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(Path));
				stream.write(file);
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}*/

	
	private void PollyDownload(String taskId, String cmsid, String filename) {
		AmazonS3 s3client;
		AWSCredentials credentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
				"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
		s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.AP_SOUTH_1).build();
		try {
			S3Object obj = s3client.getObject("et-polly", taskId +".mp3");
			S3ObjectInputStream s3is = obj.getObjectContent();
			FileOutputStream fos = new FileOutputStream(
					new File("/dev/shm/audio/" + cmsid +".mp3"));
			byte[] read_buf = new byte[1024];
			int read_len = 0;
			while ((read_len = s3is.read(read_buf)) > 0) {
				fos.write(read_buf, 0, read_len);
			}
			s3is.close();
			fos.close();
		} catch (AmazonServiceException e) {
			System.err.println(e.getErrorMessage());
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
}