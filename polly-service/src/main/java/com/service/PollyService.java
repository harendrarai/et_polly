package com.service;

import java.io.InputStream;
import java.util.List;

import com.amazonaws.services.polly.model.OutputFormat;

public interface PollyService {

	public void initialize();

	public InputStream synthesize(String text, OutputFormat format);
	public void synthesizeList(List<String> cmsDataList, OutputFormat format,String cmsid);
	public void synthesizeData(String cmsData, OutputFormat format,String cmsid,String filename);

}
