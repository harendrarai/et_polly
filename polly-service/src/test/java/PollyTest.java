import static org.awaitility.Awaitility.await;

import java.util.concurrent.TimeUnit;

import org.awaitility.Duration;

import com.Constants;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.polly.AmazonPolly;
import com.amazonaws.services.polly.AmazonPollyClient;
import com.amazonaws.services.polly.AmazonPollyClientBuilder;
import com.amazonaws.services.polly.model.GetSpeechSynthesisTaskRequest;
import com.amazonaws.services.polly.model.GetSpeechSynthesisTaskResult;
import com.amazonaws.services.polly.model.OutputFormat;
import com.amazonaws.services.polly.model.StartSpeechSynthesisTaskRequest;
import com.amazonaws.services.polly.model.StartSpeechSynthesisTaskResult;
import com.amazonaws.services.polly.model.SynthesisTask;
import com.amazonaws.services.polly.model.TaskStatus;
import com.amazonaws.services.polly.model.TextType;
import com.amazonaws.services.polly.model.VoiceId;

public class PollyTest {
	
	public static final int SYNTHESIS_TASK_TIMEOUT_SECONDS = 30000;
	public static AmazonPolly AMAZON_POLLY_CLIENT;
	public static final String OUTPUT_FORMAT_MP3 = OutputFormat.Mp3.toString();
	//public static final String OUTPUT_BUCKET = "synth-books-buckets";
	//public static final String SNS_TOPIC_ARN = "arn:aws:sns:eu-west-2:561828872312:synthesize-finish-topic";
	public static final Duration SYNTHESIS_TASK_POLL_INTERVAL = Duration.FIVE_SECONDS;
	public static final Duration SYNTHESIS_TASK_POLL_DELAY = Duration.TEN_SECONDS;
	
	private static String accessKey = "AKIAIKNFHRLGZK4B3NVA";
	private static String secretKey = "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";
	private static String OUTPUT_BUCKET = "et-polly";
	private static String SNS_TOPIC_ARN = "arn:aws:sns:ap-south-1:406154381445:speech";
	
	
public static void main(String args[]){
	System.out.println("111111111111111111111111111111");
	try {	
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
				"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
		System.out.println("aaaaaaaaaaaaaaaaaaa");

		
		AMAZON_POLLY_CLIENT = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				.withRegion(Region.getRegion(Regions.AP_SOUTH_1).getName()).build();
		System.out.println("bbbbbbbbbbbbbbbbbbbbbb");
		//AMAZON_POLLY_CLIENT = AmazonPollyClientBuilder.defaultClient();

		
		StartSpeechSynthesisTaskRequest request = new StartSpeechSynthesisTaskRequest()
                .withOutputFormat(OUTPUT_FORMAT_MP3)
                .withText("Russia has been the border with Ukraine since August and now poses the greatest military threat since 2014, the year Moscow annexed Crimea, the commander of Ukraine's armed forces told Reuters in an interview on Tuesday. General Viktor Muzhenko gestured to a series of satellite images which he said showed the presence of Russian T-62 M tanks stationed 18 km (11 miles) from the Ukrainian border. ")
                .withTextType(TextType.Text)
                .withVoiceId(VoiceId.Raveena)
                .withOutputS3BucketName(OUTPUT_BUCKET)
                .withSnsTopicArn(SNS_TOPIC_ARN);
		System.out.println("22222222222222 ");
        StartSpeechSynthesisTaskResult result = AMAZON_POLLY_CLIENT.startSpeechSynthesisTask(request);
        String taskId = result.getSynthesisTask().getTaskId();
        System.out.println("33333333333333333" + taskId);
        
		
//		String taskId = "21299c24-a6af-49bb-9e01-399ce500de94";
//		String taskId = "dd4256fa-9ede-45d2-a1ec-6015d30dc92c";

        await().with()
                .pollInterval(SYNTHESIS_TASK_POLL_INTERVAL)
                .pollDelay(SYNTHESIS_TASK_POLL_DELAY)
                .atMost(SYNTHESIS_TASK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .until(
                        () -> getSynthesisTaskStatus(taskId).equals(TaskStatus.Completed.toString())
                );
        
        System.out.println(getSynthesisTask(taskId));
        
        
        System.out.println("44444444444444");
	} catch (Exception e) {
		e.printStackTrace();
	}
}


private static SynthesisTask getSynthesisTask(String taskId) {
	
	BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
			"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
	

	
	AMAZON_POLLY_CLIENT = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
			.withRegion(Region.getRegion(Regions.AP_SOUTH_1).getName()).build();
	
    GetSpeechSynthesisTaskRequest getSpeechSynthesisTaskRequest = new GetSpeechSynthesisTaskRequest()
            .withTaskId(taskId);
    GetSpeechSynthesisTaskResult result = AMAZON_POLLY_CLIENT.getSpeechSynthesisTask(getSpeechSynthesisTaskRequest);
    return result.getSynthesisTask();
}

private static String getSynthesisTaskStatus(String taskId) {
	
	BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAITPGY6GDPMP5KBJA",
			"ox1c5gr8GI3EtRQyPk01LJFD2A2p/GRVWxDqul3m");
	
	 AmazonPolly AMAZON_POLLY_CLIENT = (AmazonPollyClient) AmazonPollyClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
			.withRegion(Region.getRegion(Regions.AP_SOUTH_1).getName()).build();
	
	
	
	GetSpeechSynthesisTaskRequest getSpeechSynthesisTaskRequest = new GetSpeechSynthesisTaskRequest()
            .withTaskId(taskId);
	
    GetSpeechSynthesisTaskResult result =AMAZON_POLLY_CLIENT.getSpeechSynthesisTask(getSpeechSynthesisTaskRequest);
    return result.getSynthesisTask().getTaskStatus();
}

}
